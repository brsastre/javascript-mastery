//1.ENCAPLUSATION
//Agrupamos variables(propiedades) y funciones(metodos) juntos, en una unidad (objeto).

//decoupled prodecural programming example:
let baseSalary = 30_000;
let overtime = 10;
let rate = 20;

function getWage(baseSalary, overtime, rate) {
    return baseSalary + (overtime * rate);
}

//coupled/encapsulated oop
let employee = {
    baseSalary = 30_000,
    overtime = 10,
    rate = 20,
    getWage() {
        return this.baseSalary + (this.overtime * this.rate);
    }
}
employee.getWage();

// ================================================
//2.ABSTRACTION
/* Un reproductor de DVD tiene botones que nos permiten interactuar :
play, stop, rewind... lo que sucede dentro del reproductor no nos importa.
Eso es abstracción.La abstracción en oop permite tener una interfaz más limpia.

Escondemos la complejidad y mostramos sólo lo esencial.
*/


// ================================================
//3.INHERITANCE
// Eliminamos la repetición del código

// ================================================
//4.POLYMORPHISM