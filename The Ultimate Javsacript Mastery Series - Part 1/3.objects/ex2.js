// let address = {
//     street: 'Lauro Muller',
//     city: 'Montevideo',
//     zipCode: 11200,
//     showAddress() {
//         console.log(`Address: ${this.city}, ${this.street}, ${this.zipCode}.`);
//     }
// }

//FACTORY FUNCTION
function createAddress(street, city, zipCode){
    return {
        street,
        city,
        zipCode
    }
}

let address = createAddress('Lauro Muller', 'Montevideo', 11200);

//CONSTRUCTOR FUNCTION
function Address(street, city, zipCode) {
    this.street = street,
    this.city = city,
    this.zipCode = zipCode
}

let address2 = new Address('Lauro Muller', 'Montevideo', 11200);

console.log(address);
console.log(address2);
