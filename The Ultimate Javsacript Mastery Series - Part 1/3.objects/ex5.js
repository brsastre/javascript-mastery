//use a constructor function to create a post object
function Post(title, body, author, views){
    this.title = title,
    this.body = body,
    this.author = author,
    this.views = 0,
    this.comments = [],
    this.isLive = false
}
let post = new Post('a', 'b', 'c')

console.log(post);