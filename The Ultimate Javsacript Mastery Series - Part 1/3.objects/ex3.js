function Address(street, city, zipCode) {
    this.street = street,
    this.city = city,
    this.zipCode = zipCode
}

let address1 = new Address('Lauro Muller', 'Montevideo', 11200);
let address2 = new Address('Lauro Muller', 'Montevideo', 11200);

//1
function areEqual(address1, address2){
    //If all the properties are the same..
    let areEqual = true
    for(let key in address1){
        if(address1[key] !== address2[key]) {
            areEqual = false;
            break;
        }
    }
    return areEqual;
}

let addressesAreEqual = areEqual(address1, address2);
console.log(addressesAreEqual);

//2
function areSame(address1, address2){
    //If they are pointing the exact same object
    return address1 === address2;
}
console.log(areSame(address1, address2));