//Create an address object:

// street
// city
// zipCode
// showAddress(address)

let address = {
    street: 'Lauro Muller',
    city: 'Montevideo',
    zipCode: 11200,
    showAddress() {
        console.log(`Address: ${this.city}, ${this.street}, ${this.zipCode}.`);
    }
}

address.showAddress();

