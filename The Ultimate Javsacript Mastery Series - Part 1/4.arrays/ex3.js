const numbers = [1, 2, 3, 4];

function except(array, excluded) {
    let filtered = [];
    for(let number of array){
        if(!excluded.includes(number))
            filtered.push(number);
    }
    return filtered;
}

const output = except(numbers, [1, 2]);
console.log(output);

