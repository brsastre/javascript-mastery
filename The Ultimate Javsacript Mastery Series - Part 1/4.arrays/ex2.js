const numbers = [1, 2, 3, 4];

//con some
function includes(array, searchElement) {
    return array.some(function(value){
        console.log(value);
        return value == searchElement;
    })
}

//con for
function includes(array, searchElement) {
    for(let number of array) {
        console.log(number);
        if(searchElement === number) {
            return true;
        }
    }
    return false;
}

let result = includes(numbers, 3);
console.log(result);