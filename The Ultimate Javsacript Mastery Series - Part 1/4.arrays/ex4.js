const numbers = [1, 2, 3, 4];

function move(array, index, offset){
    let movedArray = [];
    let newPosition = index + offset;
    if(newPosition > array.length - 1 || newPosition < 0) {
        console.error('Invalid offset');
        return;
    }
    
    for(let i = 0; i < array.length; i++) {
        if(i !== newPosition) {
            if(i === index) {
                movedArray.push(array[newPosition]);
            } else {
                movedArray.push(array[i]);
            }
        } else {
            movedArray.push(array[index]);
        }
    }
    return movedArray;
}

//Con splice
function move(array, index, offset){

    let newPosition = index + offset;
    if(newPosition > array.length - 1 || newPosition < 0) {
        console.error('Invalid offset');
        return;
    }
    
   let output = [...array];
   let element = output.slice(index,1)[0];

}

let output = move(numbers, 2, -2);
console.log(output);