const array = [undefined,'',    1,2,3];

function countTruthy(array) {
    let truthyValues = 0;
    for(item of array)
        if(item) truthyValues++;
    return truthyValues;
}

console.log(countTruthy(array));