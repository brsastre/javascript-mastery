//Speed limit = 70 -> ok message
//5 -> 1 point 
//Math.floor(1.3)
//More than 12 points, license suspended

function checkSpeed(speed) {
    const speedLimit = 70;
    const kilometerPerPoint = 5;
    if(speed < speedLimit + kilometerPerPoint)
        return "Ok";

    let speedExcess = Math.floor(speed) - speedLimit;
    points = Math.floor(speedExcess / kilometerPerPoint);
    return points > 12 ? "License suspended!" : points  
    
}

console.log(checkSpeed(135));
